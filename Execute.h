/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file Execute.h
 */

#ifndef EXECUTE_H
#define EXECUTE_H

// KDE includes
#include <KDialog>

// Forward declarations
class Rom;
class KComboBox;
class QGroupBox;

/**
 * \class Execute
 * \brief Dialog to edit a ROM.
 */
class Execute : public KDialog
{
    Q_OBJECT
    
    public:
        /**
         * \brief Constructor.
         * 
         * \param parent The parent of the editor.
         */
        Execute(QWidget* parent, Rom* rom);
    protected slots:
        void emulatorChanged(const QString& emulator);
        
        void execute();
    private:
        Rom* m_rom;
        KComboBox* m_emulator;
        QGroupBox* m_useProfile;
        KComboBox* m_profile;
};

#endif
