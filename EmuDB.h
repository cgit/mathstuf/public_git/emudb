/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file EmuDB.h
 */

#ifndef EMUDB_H
#define EMUDB_H

// KDE includes
#include <KMainWindow>

// Forward declarations
class QTreeWidget;
class QTreeWidgetItem;

/**
 * \class EmuDB
 * \brief The main window for %EmuDB
 */
class EmuDB : public KMainWindow
{
    Q_OBJECT
    
    public:
        /**
         * \brief Default constructor.
         */
        EmuDB();
    public slots:
        void addRom();
        void removeRom(const int index);
        
        void addRoms();
        void addFiles(const QString& path);
        void addFile(const QString& path);
        
        void editRom();
        void removeRoms();
        
        void execute(QTreeWidgetItem* item);
        void execute();
        
        /**
         * \brief Edit the emulators in the database.
         */
        void editEmulators();
        
        /**
         * \brief Edit the ROM types in the database.
         */
        void editRomTypes();
    private:
        QTreeWidget* m_view;
};

#endif
