/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file Rom.h
 */

#ifndef ROM_H
#define ROM_H

// Qt includes
#include <QtCore/QObject>
#include <QtCore/QString>

// Forward declarations
class KConfigGroup;

/**
 * \class Rom
 * \brief Details a ROM.
 */
class Rom : public QObject
{
    Q_OBJECT
    /**
     * \var name
     * \brief The name of the ROM.
     * 
     * \sa name
     * \sa setName
     */
    Q_PROPERTY(QString name READ name WRITE setName)
    /**
     * \var path
     * \brief The location of the ROM.
     * 
     * \sa path
     * \sa setPath
     */
    Q_PROPERTY(QString path READ path WRITE setPath)
    /**
     * \var type
     * \brief The type of the ROM.
     * 
     * \sa type
     * \sa setType
     */
    Q_PROPERTY(QString type READ type WRITE setType)
    /**
     * \var rating
     * \brief The rating of the ROM on a scale of 1-10.
     * 
     * \sa rating
     * \sa setRating
     */
    Q_PROPERTY(int rating READ rating WRITE setRating)
    /**
     * \var notes
     * \brief Notes on the ROM.
     * 
     * \sa notes
     * \sa setNotes
     */
    Q_PROPERTY(QString notes READ notes WRITE setNotes)
    
    public:
        /**
         * \brief Default constructor.
         */
        Rom();
        
        /**
         * \brief Config constructor.
         */
        Rom(const KConfigGroup& config);
        
        /**
         * \brief Copy constructor.
         * 
         * \param rhs The ROM to copy.
         */
        Rom(const Rom& rhs);
        
        /**
         * \brief Create a KConfigGroup for saving.
         * 
         * \param parent The parent group.
         */
        void makeGroup(const KConfigGroup& parent) const;
        
        /**
         * \return The name of the ROM.
         */
        QString name() const;
        
        /**
         * \return The location of the ROM.
         */
        QString path() const;
        
        /**
         * \return The type of the ROM.
         */
        QString type() const;
        
        /**
         * \return The rating of the ROM.
         */
        int rating() const;
        
        /**
         * \return Notes anout the ROM.
         */
        QString notes() const;
        
        /**
         * \brief Assignment method.
         * 
         * \param rhs ROM to copy.
         */
        Rom& operator=(const Rom& rhs);
    public slots:
        /**
         * \brief Set the name of the ROM.
         * 
         * \param name 
         */
        void setName(const QString& name);
        
        /**
         * \brief Set the location of the ROM.
         * 
         * \param path 
         */
        void setPath(const QString& path);
        
        /**
         * \brief Set the type of the ROM.
         * 
         * \param type 
         */
        void setType(const QString& type);
        
        /**
         * \brief Set the rating.
         * 
         * \param rating
         */
        void setRating(const int rating);
        
        /**
         * \brief Set the notes about the ROM.
         * 
         * \param notes 
         */
        void setNotes(const QString& notes);
    private:
        QString m_name;
        QString m_path;
        QString m_type;
        int m_rating;
        QString m_notes;
};

#endif
