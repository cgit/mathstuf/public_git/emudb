/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file EmuDBConfig.cpp
 */

// Header include
#include "EmuDBConfig.h"

// EmuDB includes
#include "Rom.h"

// KDE includes
#include <KConfigGroup>
#include <KGlobal>
#include <KSharedConfig>

EmuDBConfig* EmuDBConfig::m_instance = NULL;
const char* EmuDBConfig::m_names[] = {"name", "type", "rating", "notes"};
const QStringList EmuDBConfig::m_headers = QStringList() << "Name" << "Type" << "Rating" << "Notes";

EmuDBConfig* EmuDBConfig::instance()
{
    if (m_instance)
        return m_instance;
    return m_instance = new EmuDBConfig;
}

void EmuDBConfig::cleanup()
{
    delete m_instance;
    m_instance = NULL;
}

bool EmuDBConfig::hasEmulator(const QString& name) const
{
    return m_emulators.contains(name);
}

QStringList EmuDBConfig::emulators() const
{
    return m_emulators.keys();
}

Emulator* EmuDBConfig::emulator(const QString& name)
{
    if (m_emulators.contains(name))
        return &m_emulators[name];
    return NULL;
}

void EmuDBConfig::addEmulator(const QString& name, const Emulator& emulator)
{
    m_emulators[name] = emulator;
}

void EmuDBConfig::removeEmulator(const QString& name)
{
    if (m_emulators.contains(name))
        m_emulators.remove(name);
}

RomLibrary* EmuDBConfig::romLibrary()
{
    return &m_romLibrary;
}

bool EmuDBConfig::hasRomType(const QString& name) const
{
    return m_romTypes.contains(name);
}

QStringList EmuDBConfig::romTypes() const
{
    return m_romTypes.keys();
}

RomType* EmuDBConfig::romType(const QString& name)
{
    if (m_romTypes.contains(name))
        return &m_romTypes[name];
    return NULL;
}

void EmuDBConfig::addRomType(const QString& name, const RomType& type)
{
    m_romTypes[name] = type;
}

void EmuDBConfig::removeRomType(const QString& name)
{
    if (m_romTypes.contains(name))
        m_romTypes.remove(name);
}

int EmuDBConfig::columnCount() const
{
    return m_columns.count();
}

QString EmuDBConfig::columnHeader(const int column) const
{
    return m_headers[m_columns[column]];
}

const char* EmuDBConfig::columnName(const int column) const
{
    return m_names[m_columns[column]];
}

EmuDBConfig::EmuDBConfig() :
        m_config(KGlobal::config())
{
    m_columns << 0 << 1 << 2 << 3;
    KConfigGroup emulators = m_config->group("Emulators");
    QStringList groups = emulators.groupList();
    foreach (const QString& group, groups)
        addEmulator(group, Emulator(emulators.group(group)));
    KConfigGroup types = m_config->group("Types");
    groups = types.groupList();
    foreach (const QString& group, groups)
        addRomType(group, RomType(types.group(group)));
    KConfigGroup roms = m_config->group("Library");
    groups = roms.groupList();
    foreach (const QString& group, groups)
        m_romLibrary.addRom(new Rom(roms.group(group)));
    if (m_config->group("EmuDB").hasKey("Columns"))
        m_columns = m_config->group("EmuDB").readEntry("Columns", QList<int>());
}

EmuDBConfig::~EmuDBConfig()
{
    QStringList names = m_emulators.keys();
    foreach (const QString& name, names)
        m_emulators[name].makeGroup(name, m_config->group("Emulators"));
    names = m_romTypes.keys();
    foreach (const QString& name, names)
        m_romTypes[name].makeGroup(name, m_config->group("Types"));
    for (int i = 0; i < m_romLibrary.size(); ++i)
        m_romLibrary.rom(i)->makeGroup(m_config->group("Library"));
    m_config->group("EmuDB").writeEntry("Columns", m_columns);
    m_config->sync();
}
