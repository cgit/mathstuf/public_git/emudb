/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file Emulator.h
 */

#ifndef EMULATOR_H
#define EMULATOR_H

// EmuDB includes
#include "Profile.h"

// Qt includes
#include <QtCore/QMap>
#include <QtCore/QMetaType>
#include <QtCore/QSet>
#include <QtCore/QString>

// Forward declarations
class Rom;

/**
 * \class Emulator
 * \brief Provides information on how to run an emulator.
 */
class Emulator : public QObject
{
    Q_OBJECT
    /**
     * \var command
     * \brief The command used to execute the emulator.
     * 
     * \sa command
     * \sa setCommand
     */
    Q_PROPERTY(QString command READ command WRITE setCommand)
    /**
     * \var types
     * \brief The types of Roms the emulator supports.
     * 
     * \sa types
     * \sa addType
     * \sa removeType
     */
    Q_PROPERTY(QSet<QString> types READ types)
    /**
     * \var profiles
     * \brief Profiles for the emulator.
     * 
     * \sa profiles
     * \sa profileNames
     * \sa setProfile
     * \sa removeProfile
     */
    Q_PROPERTY(ProfileMap profiles READ profiles)
    
    public:
        /**
         * \typedef ProfileMap
         * \brief Storage type for the profiles.
         */
        typedef QMap<QString, Profile> ProfileMap;
        
        /**
         * \brief Default constructor.
         */
        Emulator();
        
        /**
         * \brief Configuration constructor.
         */
        Emulator(const KConfigGroup& config);
        
        /**
         * \brief Copy constructor.
         * 
         * \param rhs The profile to copy.
         */
        Emulator(const Emulator& rhs);
        
        /**
         * \brief Create a KConfigGroup for saving.
         * 
         * \param name The name of the group.
         * \param parent The parent group.
         */
        void makeGroup(const QString& name, const KConfigGroup& parent) const;
        
        /**
         * \return The command used to run the emulator.
         */
        QString command() const;
        
        /**
         * \return The systems the emulator can run.
         */
        QSet<QString> types() const;
        
        /**
         * \return All profiles for the emulator.
         */
        ProfileMap profiles() const;
        
        /**
         * \return Names of all profiles for the emulator.
         */
        QStringList profileNames() const;
        
        /**
         * \param name The name of the profile.
         * \return The profile with the given name, or default if it does not exist.
         */
        Profile* profile(const QString& name);
        
        /**
         * \brief Assignment method.
         * \param rhs Emulator to copy.
         */
        Emulator& operator=(const Emulator& rhs);
    public slots:
        /**
         * \brief Set the command used to run the emulator.
         * 
         * \param command 
         */
        void setCommand(const QString& command);
        
        /**
         * \brief Add a type that the emulator can run.
         * 
         * \param type 
         */
        void addType(const QString& type);
        
        /**
         * \brief Remove a type from the emulator's support.
         * 
         * \param type 
         */
        void removeType(const QString& type);
        
        /**
         * \brief Add a profile.
         * 
         * \param name The name of the profile.
         * \param profile The profile
         */
        void addProfile(const QString& name, const Profile& profile);
        
        /**
         * \brief Delete a profile for the emulator.
         * 
         * \param name The profile to remove.
         */
        void removeProfile(const QString& name);
        
        /**
         * \brief Run the emulator.
         * 
         * \param profileName The profile to use.
         * \param rom The Rom to play.
         */
        void execute(const QString& profileName, const Rom& rom);
    private:
        QString m_command;
        QSet<QString> m_types;
        ProfileMap m_profiles;
};
Q_DECLARE_METATYPE(Emulator*)

#endif
