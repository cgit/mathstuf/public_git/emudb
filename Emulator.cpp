/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file Emulator.cpp
 */

// Header include
#include "Emulator.h"

// EmuDB includes
#include "Rom.h"

// KDE includes
#include <KConfigGroup>
#include <KMessageBox>

// Qt includes
#include <QtCore/QDir>
#include <QtCore/QProcess>

#include <QtCore/QtDebug>

Emulator::Emulator() :
        QObject(NULL)
{
}

Emulator::Emulator(const KConfigGroup& config) :
        QObject(NULL)
{
    setCommand(config.readEntry("command", ""));
    m_types = QSet<QString>::fromList(config.readEntry("types", QList<QString>()));
    QStringList groups = config.group("Profiles").groupList();
    foreach (const QString& group, groups)
        addProfile(group, Profile(config.group("Profiles").group(group)));
}

Emulator::Emulator(const Emulator& rhs) :
        QObject(NULL)
{
    *this = rhs;
}

void Emulator::makeGroup(const QString& name, const KConfigGroup& parent) const
{
    KConfigGroup config(&parent, name);
    config.writeEntry("command", m_command);
    config.writeEntry("types", QList<QString>::fromSet(m_types));
    QStringList profileNames = m_profiles.keys();
    foreach (const QString& profileName, profileNames)
        m_profiles[profileName].makeGroup(profileName, config.group("Profiles"));
}

void Emulator::setCommand(const QString& command)
{
    m_command = command;
}

void Emulator::addType(const QString& type)
{
    m_types.insert(type);
}

void Emulator::removeType(const QString& type)
{
    if (m_types.contains(type))
        m_types.remove(type);
}

void Emulator::addProfile(const QString& name, const Profile& profile)
{
    m_profiles[name] = profile;
}

void Emulator::removeProfile(const QString& name)
{
    if (m_profiles.contains(name))
        m_profiles.remove(name);
}

void Emulator::execute(const QString& profileName, const Rom& rom)
{
    if (!m_types.contains(rom.type()))
    {
        if (KMessageBox::questionYesNo(NULL, "The Rom type is not supported by the emulator. Continue anyways?", "Incompatable Rom") == KMessageBox::No)
            return;
    }
    QProcess process;
    Profile* prof = profile(profileName);
    QString cmd = m_command;
    if (prof)
    {
        qDebug("Setting Profile");
        qDebug() << cmd;
        cmd.append(QString(' ').append(prof->options().join(" ")));
        qDebug() << cmd;
        process.setEnvironment(prof->environmentList());
        process.setWorkingDirectory(prof->workingPath());
    }
    QString romPath = rom.path();
    cmd.append(QString(" \"%1\"").arg(rom.path()));
    qDebug() << cmd;
    process.start(cmd);
    while (!process.waitForStarted())
    {
        if (process.error() == QProcess::Timedout)
            continue;
        QString error;
        QString details = process.readAll();
        switch (process.error())
        {
            case QProcess::FailedToStart:
                error = "Failed to start";
                break;
            case QProcess::Crashed:
                error = "Crashed";
                break;
            case QProcess::ReadError:
                error = "Read error";
                break;
            case QProcess::WriteError:
                error = "Write error";
                break;
            default:
                error = "Unknown error";
                break;
        }
        KMessageBox::detailedError(NULL, QString("Command executed:\n%1\nWorking Directory:\n%2\nEnvironment:\n%3").arg(cmd).arg(prof->workingPath()).arg(prof->environmentList().join("\n")), details, error, KMessageBox::Notify | KMessageBox::AllowLink);
    }
    qDebug() << process.readAll();
    qDebug() << "Waiting";
    process.waitForFinished(-1);
    qDebug() << "Done";
}

QString Emulator::command() const
{
    return m_command;
}

QSet<QString> Emulator::types() const
{
    return m_types;
}

Emulator::ProfileMap Emulator::profiles() const
{
    return m_profiles;
}

QStringList Emulator::profileNames() const
{
    return m_profiles.keys();
}

Profile* Emulator::profile(const QString& name)
{
    if (m_profiles.contains(name))
        return &m_profiles[name];
    return NULL;
}

Emulator& Emulator::operator=(const Emulator& rhs)
{
    if (this == &rhs)
        return *this;
    m_command = rhs.m_command;
    m_types = rhs.m_types;
    m_profiles = rhs.m_profiles;
    return *this;
}
