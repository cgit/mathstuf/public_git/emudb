/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file EmuDB.cpp
 */

// Header include
#include "EmuDB.h"

// EmuDB includes
#include "EmuDBConfig.h"
#include "EmulatorEditor.h"
#include "Execute.h"
#include "Rom.h"
#include "RomEditor.h"
#include "RomModel.h"
#include "RomTypeEditor.h"

// KDE includes
#include <KAction>
#include <KFileDialog>
#include <KMenu>
#include <KMenuBar>
#include <KPushButton>
#include <KStandardAction>
#include <KTreeWidgetSearchLine>

// Qt includes
#include <QtCore/QFileInfo>
#include <QtGui/QHBoxLayout>
#include <QtGui/QSortFilterProxyModel>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>

#include <QtCore/QtDebug>

EmuDB::EmuDB() :
        m_view(new QTreeWidget)
{
    m_view->setSortingEnabled(true);
    connect(m_view, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(execute(QTreeWidgetItem*)));
    
    KTreeWidgetSearchLine* search = new KTreeWidgetSearchLine;
    search->setTreeWidget(m_view);
    search->setClearButtonShown(true);
    
    KPushButton* exec = new KPushButton;
    exec->setText("Execute");
    connect(exec, SIGNAL(pressed()), this, SLOT(execute()));
    KPushButton* edit = new KPushButton;
    edit->setText("Edit");
    connect(edit, SIGNAL(pressed()), this, SLOT(editRom()));
    KPushButton* add = new KPushButton;
    add->setText("Add");
    connect(add, SIGNAL(pressed()), this, SLOT(addRoms()));
    KPushButton* remove = new KPushButton;
    remove->setText("Remove");
    connect(remove, SIGNAL(pressed()), this, SLOT(removeRoms()));
    
    QWidget* buttons = new QWidget;
    QHBoxLayout* bLayout = new QHBoxLayout;
    bLayout->addWidget(exec);
    bLayout->addWidget(edit);
    bLayout->addWidget(add);
    bLayout->addWidget(remove);
    buttons->setLayout(bLayout);
    
    QWidget* widget = new QWidget;
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(search);
    layout->addWidget(m_view);
    layout->addWidget(buttons);
    widget->setLayout(layout);
    
    setCentralWidget(widget);
    
    RomLibrary* library = EmuDBConfig::instance()->romLibrary();
    
    connect(library, SIGNAL(added()), this, SLOT(addRom()));
    connect(library, SIGNAL(removed(int)), this, SLOT(removeRom(int)));
    
    for (int i = 0; i < library->size(); ++i)
        new RomModel(m_view->invisibleRootItem(), library->rom(i));
    
    m_view->setHeaderLabels(EmuDBConfig::m_headers);
    setAutoSaveSettings("MainWindow", true);
    
    setMenuBar(new KMenuBar(this));
    KMenu* fileMenu = new KMenu("&File", this);
    KAction* addFiles = new KAction("&Add files", this);
    addFiles->setShortcut(Qt::CTRL + Qt::Key_A);
    connect(addFiles, SIGNAL(triggered()), this, SLOT(addRoms()));
    fileMenu->addAction(addFiles);
    fileMenu->addSeparator();
    fileMenu->addAction(KStandardAction::quit(this, SLOT(close()), this));
    menuBar()->addMenu(fileMenu);
    KMenu* editMenu = new KMenu("&Edit", this);
    KAction* editEmulator = new KAction("Edit &emulators", this);
    editEmulator->setShortcut(Qt::CTRL + Qt::Key_E);
    connect(editEmulator, SIGNAL(triggered()), this, SLOT(editEmulators()));
    editMenu->addAction(editEmulator);
    KAction* editTypes = new KAction("Edit &ROM types", this);
    editTypes->setShortcut(Qt::CTRL + Qt::Key_R);
    connect(editTypes, SIGNAL(triggered()), this, SLOT(editRomTypes()));
    editMenu->addAction(editTypes);
    menuBar()->addMenu(editMenu);
    menuBar()->addMenu(helpMenu());
}

void EmuDB::addRoms()
{
    QStringList paths = KFileDialog::getOpenFileNames();
    foreach (const QString& path, paths)
    {
        QFileInfo info(path);
        if (info.isDir())
            addFiles(info.filePath());
        else
            addFile(info.filePath());
    }
}

void EmuDB::addFiles(const QString& path)
{
    QDir dir(path);
    QFileInfoList entries = dir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
    foreach (const QFileInfo& entry, entries)
    {
        if (entry.isDir())
            addFiles(entry.filePath());
        else
            addFile(entry.filePath());
    }
}

void EmuDB::addFile(const QString& path)
{
    QStringList types = EmuDBConfig::instance()->romTypes();
    RomLibrary* library = EmuDBConfig::instance()->romLibrary();
    QFileInfo info(path);
    QString suffix = info.suffix();
    foreach (const QString& type, types)
    {
        if (EmuDBConfig::instance()->romType(type)->extensions().contains(suffix))
        {
            Rom* rom = new Rom;
            rom->setName(info.completeBaseName());
            rom->setPath(info.absoluteFilePath());
            rom->setType(type);
            library->addRom(rom);
            break;
        }
    }
}

void EmuDB::editRom()
{
    RomModel* romModel = static_cast<RomModel*>(m_view->currentItem());
    if (!romModel)
        return;
    RomEditor* editor = new RomEditor(this, romModel->rom());
    editor->exec();
    delete editor;
}

void EmuDB::removeRoms()
{
    QList<QTreeWidgetItem*> items = m_view->selectedItems();
    foreach (QTreeWidgetItem* item, items)
    {
        RomModel* romModel = static_cast<RomModel*>(item);
        if (romModel)
            EmuDBConfig::instance()->romLibrary()->removeRom(romModel->rom());
    }
}

void EmuDB::execute(QTreeWidgetItem* item)
{
    RomModel* romModel = static_cast<RomModel*>(item);
    if (!romModel)
        return;
    Execute* exec = new Execute(this, romModel->rom());
    exec->exec();
    delete exec;
}

void EmuDB::execute()
{
    execute(m_view->currentItem());
}

void EmuDB::editEmulators()
{
    EmulatorEditor* editor = new EmulatorEditor(this);
    editor->exec();
    delete editor;
}

void EmuDB::editRomTypes()
{
    RomTypeEditor* editor = new RomTypeEditor(this);
    editor->exec();
    delete editor;
}

void EmuDB::addRom()
{
    RomLibrary* library = EmuDBConfig::instance()->romLibrary();
    new RomModel(m_view->invisibleRootItem(), library->rom(library->size() - 1));
}

void EmuDB::removeRom(const int index)
{
    delete m_view->invisibleRootItem()->takeChild(index);
}
