/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file RomType.cpp
 */

// Header include
#include "RomType.h"

// EmuDB includes
#include "EmuDBConfig.h"

// KDE includes
#include <KConfigGroup>

RomType::RomType() :
        QObject(NULL)
{
}

RomType::RomType(const KConfigGroup& config) :
        QObject(NULL)
{
    m_extensions = QSet<QString>::fromList(config.readEntry("extensions", QList<QString>()));
}

RomType::RomType(const RomType& rhs) :
        QObject(NULL)
{
    *this = rhs;
}

void RomType::makeGroup(const QString& name, const KConfigGroup& parent) const
{
    KConfigGroup config(&parent, name);
    config.writeEntry("extensions", QList<QString>::fromSet(m_extensions));
}

void RomType::addExtension(const QString& extension)
{
    m_extensions.insert(extension);
}

void RomType::removeExtension(const QString& extension)
{
    if (m_extensions.contains(extension))
        m_extensions.remove(extension);
}

QSet<QString> RomType::extensions() const
{
    return m_extensions;
}

RomType& RomType::operator=(const RomType& rhs)
{
    if (this == &rhs)
        return *this;
    m_extensions = rhs.m_extensions;
    return *this;
}
