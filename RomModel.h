/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file RomModel.h
 */

#ifndef ROMMODEL_H
#define ROMMODEL_H

// Qt includes
#include <QtGui/QTreeWidgetItem>

// Forward declarations
class Rom;
class RomLibrary;

/**
 * \class RomModel
 * \brief Model used to view the library.
 */
class RomModel : public QTreeWidgetItem
{
    public:
        /**
         * \brief Default constructor.
         */
        RomModel(QTreeWidgetItem* parent, Rom* rom);
        
        /**
         * \return Flags for the model.
         */
        Qt::ItemFlags flags() const;
        
        /**
         * \brief Set data in the list.
         * 
         * \param index The index to set the data.
         * \param role What part of the data to set.
         * \param value The data.
         */
        void setData(const int column, const int role, const QVariant& value);
        
        /**
         * \param column The column to get the data from.
         * \param role What part of the data to get.
         * \return The data.
         */
        QVariant data(const int column, int role = Qt::DisplayRole) const;
        
        Rom* rom();
    private:
        Rom* m_rom;
};

#endif
