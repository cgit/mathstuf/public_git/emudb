/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file EmuDBConfig.h
 */

#ifndef EMUDBCONFIG_H
#define EMUDBCONFIG_H

// EmuEB includes
#include "Emulator.h"
#include "RomLibrary.h"
#include "RomType.h"

// KDE includes
#include <KSharedConfig>

/**
 * \class EmuDBConfig
 * \brief Singleton to access configuration options.
 */
class EmuDBConfig
{
    public:
        /**
         * \brief Used to access the configuration.
         * 
         * \return A pointer to an instance of the configuration.
         */
        static EmuDBConfig* instance();
        
        /**
         * \brief Cleans up the configuration.
         * 
         * \note Call before closing the application.
         */
        static void cleanup();
        
        /**
         * \param name The name of the emulator.
         * \return Whether the emulator exists or not.
         */
        bool hasEmulator(const QString& name) const;
        
        /**
         * \return List of emulators.
         */
        QStringList emulators() const;
        
        /**
         * \param name Name of the emulator.
         * \return An emulator.
         */
        Emulator* emulator(const QString& name);
        
        /**
         * \brief Add an emulator to the database.
         * 
         * \param name The name of the emulator.
         * \param emulator The emulator.
         */
        void addEmulator(const QString& name, const Emulator& emulator);
        
        /**
         * \brief Remove an emulator from the database.
         * 
         * \param name The name of the emulator.
         */
        void removeEmulator(const QString& name);
        
        /**
         * \return A pointer to the ROM library.
         */
        RomLibrary* romLibrary();
        
        /**
         * \param name The name of the ROM type.
         * \return Whether the ROM type exists or not.
         */
        bool hasRomType(const QString& name) const;
        
        /**
         * \return List of known ROM types.
         */
        QStringList romTypes() const;
        
        /**
         * \param name Name of the ROM type.
         * \return A ROM type.
         */
        RomType* romType(const QString& name);
        
        /**
         * \brief Add a ROM type to the database.
         * 
         * \param name The name of the type.
         * \param type The type.
         */
        void addRomType(const QString& name, const RomType& type);
        
        /**
         * \brief Remove a ROM type from the database.
         * 
         * \param name The name of the type.
         */
        void removeRomType(const QString& name);
        
        /**
         * \return The number of columns in the view.
         */
        int columnCount() const;
        
        /**
         * \param column The column.
         * \return The string for the header of the column.
         */
        QString columnHeader(const int column) const;
        
        /**
         * \param column The column.
         * \return The name of the poroperty for the column.
         */
        const char* columnName(const int column) const;
        
        static const char* m_names[];
        static const QStringList m_headers;
    private:
        EmuDBConfig();
        ~EmuDBConfig();
        
        static EmuDBConfig* m_instance;
        
        KSharedConfigPtr m_config;
        RomLibrary m_romLibrary;
        QMap<QString, Emulator> m_emulators;
        QMap<QString, RomType> m_romTypes;
        QList<int> m_columns;
};

#endif
