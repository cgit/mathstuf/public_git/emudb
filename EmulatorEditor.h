/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file EmulatorEditor.h
 */

#ifndef EMULATOREDITOR_H
#define EMULATOREDITOR_H

// KDE includes
#include <KDialog>

// Forward declarations
class Emulator;
class Profile;
class QListView;
class QModelIndex;

/**
 * \class EmulatorEditor
 * \brief Dialog to edit an emulator.
 */
class EmulatorEditor : public KDialog
{
    Q_OBJECT
    
    public:
        /**
         * \brief Constructor.
         * 
         * \param parent The parent of the editor.
         */
        EmulatorEditor(QWidget* parent);
    public slots:
        void editEmulator(const QModelIndex& index);
        
        void editEmulator(Emulator* emulator);
        
        void addEmulator(const QString& name);
        
        void removeEmulator(const QString& name);
        
        void editProfile(const QModelIndex& index);
        
        void editProfile(Profile* profile);
        
        void addProfile(const QString& name);
        
        void removeProfile(const QString& name);
    private:
        QListView* m_emulators;
        Emulator* m_emulator;
        QListView* m_profiles;
};

#endif
