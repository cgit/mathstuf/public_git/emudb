/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file Execute.cpp
 */

// Header include
#include "Execute.h"

// EmuDB includes
#include "EmuDBConfig.h"
#include "Emulator.h"
#include "Rom.h"

// KDE includes
#include <KComboBox>

// Qt includes
#include <QtGui/QGroupBox>
#include <QtGui/QVBoxLayout>

Execute::Execute(QWidget* parent, Rom* rom) :
        KDialog(parent),
        m_rom(rom)
{
    connect(this, SIGNAL(accepted()), SLOT(execute()));
    
    setCaption("Execute ROM");
    
    QGroupBox* emulatorGroup = new QGroupBox("Emulator");
    
    m_profile = new KComboBox(false);
    
    m_emulator = new KComboBox(false);
    connect(m_emulator, SIGNAL(currentIndexChanged(QString)), this, SLOT(emulatorChanged(QString)));
    m_emulator->addItems(EmuDBConfig::instance()->emulators());
    
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(m_emulator);
    emulatorGroup->setLayout(layout);
    
    m_useProfile = new QGroupBox("Profile");
    m_useProfile->setCheckable(true);
    
    layout = new QVBoxLayout;
    layout->addWidget(m_profile);
    m_useProfile->setLayout(layout);
    
    QWidget* widget = new QWidget;
    layout = new QVBoxLayout;
    layout->addWidget(emulatorGroup);
    layout->addWidget(m_useProfile);
    widget->setLayout(layout);
    
    setMainWidget(widget);
}

void Execute::emulatorChanged(const QString& emulator)
{
    m_profile->clear();
    m_profile->addItems(EmuDBConfig::instance()->emulator(emulator)->profileNames());
}

void Execute::execute()
{
    EmuDBConfig::instance()->emulator(m_emulator->currentText())->execute(m_useProfile->isChecked() ? m_profile->currentText() : "", *m_rom);
}
