/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file Rom.cpp
 */

// Header include
#include "Rom.h"

// KDE includes
#include <KConfigGroup>

// Qt includes
#include <QtCore/QFile>

Rom::Rom() :
        QObject(NULL),
        m_rating(5)
{
}

Rom::Rom(const KConfigGroup& config) :
        QObject(NULL)
{
    setName(config.readEntry("name", ""));
    setPath(config.readEntry("path", ""));
    setType(config.readEntry("type", ""));
    setRating(config.readEntry("rating", 5));
    setNotes(config.readEntry("notes", ""));
}

Rom::Rom(const Rom& rhs) :
        QObject(NULL)
{
    *this = rhs;
}

void Rom::makeGroup(const KConfigGroup& parent) const
{
    KConfigGroup config(&parent, m_name);
    config.writeEntry("name", m_name);
    config.writeEntry("path", m_path);
    config.writeEntry("type", m_type);
    config.writeEntry("rating", m_rating);
    config.writeEntry("notes", m_notes);
}

void Rom::setName(const QString& name)
{
    m_name = name;
}

void Rom::setPath(const QString& path)
{
    if (QFile::exists(path))
        m_path = path;
}

void Rom::setType(const QString& type)
{
    m_type = type;
}

void Rom::setRating(const int rating)
{
    if ((0 < rating) && (rating < 11))
        m_rating = rating;
}

void Rom::setNotes(const QString& notes)
{
    m_notes = notes;
}

QString Rom::name() const
{
    return m_name;
}

QString Rom::path() const
{
    return m_path;
}

QString Rom::type() const
{
    return m_type;
}

int Rom::rating() const
{
    return m_rating;
}

QString Rom::notes() const
{
    return m_notes;
}

Rom& Rom::operator=(const Rom& rhs)
{
    if (this == &rhs)
        return *this;
    m_name = rhs.m_name;
    m_path = rhs.m_path;
    m_type = rhs.m_type;
    m_notes = rhs.m_notes;
    return *this;
}
