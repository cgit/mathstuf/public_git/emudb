/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file RomLibrary.h
 */

#ifndef ROMLIBRARY_H
#define ROMLIBRARY_H

// Qt includes
#include <QtCore/QObject>
#include <QtCore/QList>

// Forward declarations
class Rom;

/**
 * \class RomLibrary
 * \brief Wrapper around a QList that signals about changes.
 */
class RomLibrary : public QObject
{
    Q_OBJECT
    
    public:
        ~RomLibrary();
        
        /**
         * \param rom The ROM to append to the list
         */
        void addRom(Rom* const & rom);
        
        /**
         * \param rom 
         */
        void removeRom(Rom* const & rom);
        
        /**
         * \return The size of the library.
         */
        int size() const;
        
        /**
         * \param row The ROM to return.
         * \return The ROM.
         */
        Rom* rom(const int row);
    signals:
        /**
         * \param index The ROM that was removed.
         */
        void removed(int index);
        
        void added();
    private:
        QList<Rom*> m_library;
};

#endif
