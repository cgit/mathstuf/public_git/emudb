/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file Profile.cpp
 */

// Header include
#include "Profile.h"

// KDE includes
#include <KConfigGroup>

// Qt includes
#include <QtCore/QDir>

Profile::Profile() :
        QObject(NULL)
{
}

Profile::Profile(const KConfigGroup& config) :
        QObject(NULL)
{
    setWorkingPath(config.readEntry("workingPath", "."));
    m_options = config.readEntry("options", QList<QString>());
    QStringList envList = config.readEntry("environment", QStringList());
    foreach (const QString& env, envList)
        addEnvironment(env);
}

Profile::Profile(const Profile& rhs) :
        QObject(NULL)
{
    *this = rhs;
}

void Profile::makeGroup(const QString& name, const KConfigGroup& parent) const
{
    KConfigGroup config(&parent, name);
    config.writeEntry("workingPath", m_workingPath);
    config.writeEntry("options", m_options);
    config.writeEntry("environment", environmentList());
}

void Profile::setWorkingPath(const QString& workingPath)
{
    if (QDir(".").exists(workingPath))
        m_workingPath = workingPath;
}

void Profile::addOption(const QString& option)
{
    m_options.append(option);
}

void Profile::removeOption(const QString& option)
{
    m_options.removeOne(option);
}

void Profile::addEnvironment(const QString& expression)
{
    const int index = expression.indexOf('=');
    m_enviroment[expression.mid(0, index)] = expression.mid(index + 1);
}

void Profile::addEnvironment(const QString& variable, const QString& value)
{
    if (!variable.contains('='))
        m_enviroment[variable] = value;
}

void Profile::removeEnvironment(const QString& variable)
{
    if (m_enviroment.contains(variable))
        m_enviroment.remove(variable);
}

QString Profile::workingPath() const
{
    return m_workingPath;
}

QStringList Profile::options() const
{
    return m_options;
}

Profile::Environment Profile::environment() const
{
    return m_enviroment;
}

QString Profile::environmentValue(const QString& variable) const
{
    if (m_enviroment.contains(variable))
        return m_enviroment[variable];
    return QString();
}

QStringList Profile::environmentList() const
{
    QStringList env;
    QList<QString> vars = m_enviroment.keys();
    foreach (const QString& var, vars)
        env << QString("%1=%2").arg(var).arg(m_enviroment[var]);
    return env;
}

Profile& Profile::operator=(const Profile& rhs)
{
    if (this == &rhs)
        return *this;
    m_workingPath = rhs.m_workingPath;
    m_options = rhs.m_options;
    m_enviroment = rhs.m_enviroment;
    return *this;
}
