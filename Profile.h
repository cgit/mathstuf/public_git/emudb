/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file Profile.h
 */

#ifndef PROFILE_H
#define PROFILE_H

// Qt includes
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>

// Forward declarations
class KConfigGroup;

/**
 * \class Profile
 * \brief Gives an evironment for an emulator.
 */
class Profile : public QObject
{
    Q_OBJECT
    /**
     * \var workingPath
     * \brief The working path when executed.
     * 
     * \sa workingPath
     * \sa setWorkingPath
     */
    Q_PROPERTY(QString workingPath READ workingPath WRITE setWorkingPath)
    /**
     * \var options
     * \brief The options for the emulator.
     * 
     * \sa options
     * \sa addOption
     * \sa removeOption
     */
    Q_PROPERTY(QStringList options READ options)
    /**
     * \var environment
     * \brief The environment for the emulator
     * 
     * \sa environment
     * \sa environmentValue
     * \sa environmentList
     */
    Q_PROPERTY(Environment environment READ environment)
    
    public:
        /**
         * \typedef Environment
         * \brief Type of the environment.
         */
        typedef QMap<QString, QString> Environment;
        
        /**
         * \brief Default constructor.
         */
        Profile();
        
        /**
         * \brief Configuration constructor.
         */
        Profile(const KConfigGroup& config);
        
        /**
         * \brief Copy constructor.
         * 
         * \param rhs The profile to copy.
         */
        Profile(const Profile& rhs);
        
        /**
         * \brief Create a KConfigGroup for saving.
         * 
         * \param name The name of the group.
         * \param parent The parent group.
         */
        void makeGroup(const QString& name, const KConfigGroup& parent) const;
        
        /**
         * \return The working path for the profile.
         */
        QString workingPath() const;
        
        /**
         * \return A list of options for the profile.
         */
        QStringList options() const;
        
        /**
         * \return A mapping of the environment.
         */
        Environment environment() const;
        
        /**
         * \param variable The variable to get.
         * \return The value of the variable.
         */
        QString environmentValue(const QString& variable) const;
        
        /**
         * \return The environment in assignment form.
         */
        QStringList environmentList() const;
        
        /**
         * \brief Assignment method.
         * \param rhs Profile to copy.
         */
        Profile& operator=(const Profile& rhs);
    public slots:
        /**
         * \brief Sets the path to run the command from.
         * 
         * \param workingPath 
         */
        void setWorkingPath(const QString& workingPath);
        
        /**
         * \brief Add an option when executed.
         * 
         * \param option 
         */
        void addOption(const QString& option);
        
        /**
         * \brief Remove an option from the profile.
         * 
         * \param option 
         */
        void removeOption(const QString& option);
        
        /**
         * \brief Add an environment expression to the profile.
         * 
         * \param expression Expression for the environment.
         */
        void addEnvironment(const QString& value);
        
        /**
         * \brief Add an environment variable to the profile.
         * 
         * \param variable The variable to set.
         * \param value The value to set it to.
         */
        void addEnvironment(const QString& variable, const QString& value);
        
        /**
         * \brief Unset an environment variable.
         * 
         * \param variable The variable to remove.
         */
        void removeEnvironment(const QString& variable);
    private:
        QString m_workingPath;
        QStringList m_options;
        Environment m_enviroment;
};

#endif
