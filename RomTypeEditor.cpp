/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file RomTypeEditor.cpp
 */

// Header include
#include "RomTypeEditor.h"

// EmuDB includes
#include "EmuDBConfig.h"
#include "RomType.h"

// KDE includes
#include <KComboBox>
#include <KEditListBox>
#include <KListWidget>
#include <KPushButton>

// Qt includes
#include <QtGui/QGroupBox>
#include <QtGui/QVBoxLayout>

RomTypeEditor::RomTypeEditor(QWidget* parent) :
        KDialog(parent)
{
    setCaption("Edit ROM Types");
    
    KEditListBox* romTypes = new KEditListBox("ROM Types", this);
    romTypes->setButtons(KEditListBox::Add | KEditListBox::Remove);
    romTypes->setItems(EmuDBConfig::instance()->romTypes());
    connect(romTypes, SIGNAL(added(QString)), this, SLOT(addRomType(QString)));
    connect(romTypes, SIGNAL(removed(QString)), this, SLOT(removeRomType(QString)));
    
    m_types = romTypes->listView();
    connect(m_types, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editRomType(QModelIndex)));
    
    setMainWidget(romTypes);
}

void RomTypeEditor::editRomType(const QModelIndex& index)
{
    editRomType(EmuDBConfig::instance()->romType(m_types->model()->data(index, Qt::DisplayRole).toString()));
}

void RomTypeEditor::editRomType(RomType* type)
{
    if (!type)
        return;
    
    KDialog* dialog = new KDialog(this);
    dialog->setCaption("Edit ROM Type");
    
    KEditListBox* extensions = new KEditListBox("Extensions", this);
    extensions->setButtons(KEditListBox::Add | KEditListBox::Remove);
    extensions->setItems(QStringList::fromSet(type->extensions()));
    connect(extensions, SIGNAL(added(QString)), type, SLOT(addExtension(QString)));
    connect(extensions, SIGNAL(removed(QString)), type, SLOT(removeExtension(QString)));
    
    dialog->setMainWidget(extensions);
    dialog->exec();
    
    delete dialog;
}

void RomTypeEditor::addRomType(const QString& name)
{
    EmuDBConfig::instance()->addRomType(name, RomType());
}

void RomTypeEditor::removeRomType(const QString& name)
{
    EmuDBConfig::instance()->removeRomType(name);
}
