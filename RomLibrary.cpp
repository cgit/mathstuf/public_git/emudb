/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file RomLibrary.cpp
 */

// Header include
#include "RomLibrary.h"

// EmuDB includes
#include "Rom.h"

RomLibrary::~RomLibrary()
{
    qDeleteAll(m_library);
}

void RomLibrary::addRom(Rom* const & rom)
{
    m_library.append(rom);
    emit(added());
}

void RomLibrary::removeRom(Rom* const & rom)
{
    const int index = m_library.indexOf(rom);
    m_library.removeOne(rom);
    emit(removed(index));
}

int RomLibrary::size() const
{
    return m_library.size();
}

Rom* RomLibrary::rom(const int row)
{
    return m_library[row];
}
