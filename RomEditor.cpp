/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file RomEditor.cpp
 */

// Header include
#include "RomEditor.h"

// EmuDB includes
#include "EmuDBConfig.h"
#include "Rom.h"

// KDE includes
#include <KComboBox>
#include <KLineEdit>
#include <KShellCompletion>

// Qt includes
#include <QtGui/QGroupBox>
#include <QtGui/QVBoxLayout>

RomEditor::RomEditor(QWidget* parent, Rom* rom) :
        KDialog(parent)
{
    setCaption("Edit ROM");
    
    QGroupBox* nameGroup = new QGroupBox("Name");
    
    KLineEdit* name = new KLineEdit(rom->name());
    connect(name, SIGNAL(textChanged(QString)), rom, SLOT(setName(QString)));
    
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(name);
    nameGroup->setLayout(layout);
    
    QGroupBox* pathGroup = new QGroupBox("Path");
    
    KLineEdit* path = new KLineEdit(rom->path());
    KShellCompletion* completion = new KShellCompletion;
    completion->setMode(KUrlCompletion::FileCompletion);
    path->setCompletionObject(completion);
    path->setAutoDeleteCompletionObject(true);
    connect(path, SIGNAL(textChanged(QString)), rom, SLOT(setPath(QString)));
    
    layout = new QVBoxLayout;
    layout->addWidget(path);
    pathGroup->setLayout(layout);
    
    QGroupBox* typeGroup = new QGroupBox("Type");
    
    KComboBox* type = new KComboBox(false);
    type->addItems(EmuDBConfig::instance()->romTypes());
    type->setCurrentIndex(type->findText(rom->type()));
    connect(type, SIGNAL(currentIndexChanged(QString)), rom, SLOT(setType(QString)));
    
    layout = new QVBoxLayout;
    layout->addWidget(type);
    typeGroup->setLayout(layout);
    
    QGroupBox* ratingGroup = new QGroupBox("Rating");
    
    QSlider* rating = new QSlider(Qt::Horizontal);
    rating->setRange(1, 10);
    rating->setValue(rom->rating());
    rating->setTickPosition(QSlider::TicksAbove);
    rating->setTickInterval(1);
    connect(rating, SIGNAL(valueChanged(int)), rom, SLOT(setRating(int)));
    
    layout = new QVBoxLayout;
    layout->addWidget(rating);
    ratingGroup->setLayout(layout);
    
    QGroupBox* notesGroup = new QGroupBox("Notes");
    
    KLineEdit* notes = new KLineEdit(rom->notes());
    connect(notes, SIGNAL(textChanged(QString)), rom, SLOT(setNotes(QString)));
    
    layout = new QVBoxLayout;
    layout->addWidget(notes);
    notesGroup->setLayout(layout);
    
    QWidget* widget = new QWidget;
    layout = new QVBoxLayout;
    layout->addWidget(nameGroup);
    layout->addWidget(pathGroup);
    layout->addWidget(typeGroup);
    layout->addWidget(ratingGroup);
    layout->addWidget(notesGroup);
    widget->setLayout(layout);
    
    setMainWidget(widget);
}
