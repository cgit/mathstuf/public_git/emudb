/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file RomModel.cpp
 */

// Header include
#include "RomModel.h"

// EmuDB includes
#include "EmuDBConfig.h"
#include "Rom.h"

RomModel::RomModel(QTreeWidgetItem* parent, Rom* rom) :
        QTreeWidgetItem(parent),
        m_rom(rom)
{
}

Qt::ItemFlags RomModel::flags() const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
}

void RomModel::setData(const int column, int role, const QVariant& value)
{
    if (role == Qt::EditRole)
        m_rom->setProperty(EmuDBConfig::instance()->columnName(column), value);
}

QVariant RomModel::data(const int column, int role) const
{
    if ((role == Qt::EditRole) || (role == Qt::DisplayRole))
        return m_rom->property(EmuDBConfig::instance()->columnName(column));
    return QVariant();
}

Rom* RomModel::rom()
{
    return m_rom;
}
