/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file RomType.h
 */

#ifndef ROMTYPE_H
#define ROMTYPE_H

// Qt includes
#include <QtCore/QMetaType>
#include <QtCore/QObject>
#include <QtCore/QSet>

// Forward declarations
class KConfigGroup;

/**
 * \class RomType
 * \brief Description of a ROM type.
 */
class RomType : public QObject
{
    Q_OBJECT
    /**
     * \var extensions
     * \brief The extensions the ROMs have.
     * 
     * \sa extensions
     * \sa addExtension
     * \sa removeExtension
     */
    Q_PROPERTY(QSet<QString> extensions READ extensions)
    
    public:
        /**
         * \brief Default constructor.
         */
        RomType();
        
        /**
         * \brief Config constructor.
         */
        RomType(const KConfigGroup& config);
        
        /**
         * \brief Copy constructor.
         */
        RomType(const RomType& rhs);
        
        /**
         * \brief Create a KConfigGroup for saving.
         * 
         * \param name The name of the group.
         * \param parent The parent group.
         */
        void makeGroup(const QString& name, const KConfigGroup& parent) const;
        
        /**
         * \return The set of extensions for the type.
         */
        QSet<QString> extensions() const;
        
        /**
         * \brief Assignment method.
         * 
         * \param rhs Type to copy.
         */
        RomType& operator=(const RomType& rhs);
    public slots:
        /**
         * \brief Adds an extension the type can have.
         * 
         * \param extension 
         */
        void addExtension(const QString& extension);
        
        /**
         * \brief Remove an extension for the type.
         * 
         * \param extension 
         */
        void removeExtension(const QString& extension);
    private:
        QSet<QString> m_extensions;
};
Q_DECLARE_METATYPE(RomType*)

#endif
