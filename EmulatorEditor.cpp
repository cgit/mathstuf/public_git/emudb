/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file EmulatorEditor.cpp
 */

// Header include
#include "EmulatorEditor.h"

// EmuDB includes
#include "EmuDBConfig.h"
#include "Emulator.h"

// KDE includes
#include <KComboBox>
#include <KEditListBox>
#include <KLineEdit>
#include <KListWidget>
#include <KPushButton>
#include <KShellCompletion>

// Qt includes
#include <QtGui/QGroupBox>
#include <QtGui/QVBoxLayout>

EmulatorEditor::EmulatorEditor(QWidget* parent) :
        KDialog(parent)
{
    setCaption("Edit Emulators");
    
    KEditListBox* emulators = new KEditListBox("Emulators", this);
    emulators->setButtons(KEditListBox::Add | KEditListBox::Remove);
    emulators->setItems(EmuDBConfig::instance()->emulators());
    connect(emulators, SIGNAL(added(QString)), this, SLOT(addEmulator(QString)));
    connect(emulators, SIGNAL(removed(QString)), this, SLOT(removeEmulator(QString)));
    
    m_emulators = emulators->listView();
    connect(m_emulators, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editEmulator(QModelIndex)));
    
    setMainWidget(emulators);
}

void EmulatorEditor::editEmulator(const QModelIndex& index)
{
    editEmulator(EmuDBConfig::instance()->emulator(m_emulators->model()->data(index, Qt::DisplayRole).toString()));
}

void EmulatorEditor::editEmulator(Emulator* emulator)
{
    if (!emulator)
        return;
    
    m_emulator = emulator;
    
    KDialog* dialog = new KDialog(this);
    dialog->setCaption("Edit Emulator");
    
    QGroupBox* commandGroup = new QGroupBox("Command");
    
    KLineEdit* command = new KLineEdit(emulator->command());
    KShellCompletion* completion = new KShellCompletion;
    completion->setMode(KUrlCompletion::ExeCompletion);
    command->setCompletionObject(completion);
    command->setAutoDeleteCompletionObject(true);
    connect(command, SIGNAL(textChanged(QString)), emulator, SLOT(setCommand(QString)));
    
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(command);
    commandGroup->setLayout(layout);
    
    KEditListBox* types = new KEditListBox("Supported Types");
    types->setButtons(KEditListBox::Add | KEditListBox::Remove);
    types->setItems(QStringList::fromSet(emulator->types()));
    KComboBox* typeBox = new KComboBox(false);
    typeBox->addItems(EmuDBConfig::instance()->romTypes());
    types->setCustomEditor(typeBox);
    connect(types, SIGNAL(added(QString)), emulator, SLOT(addType(QString)));
    connect(types, SIGNAL(removed(QString)), emulator, SLOT(removeType(QString)));
    
    KEditListBox* profiles = new KEditListBox("Profiles");
    profiles->setButtons(KEditListBox::Add | KEditListBox::Remove);
    profiles->setItems(emulator->profileNames());
    connect(profiles, SIGNAL(added(QString)), this, SLOT(addProfile(QString)));
    connect(profiles, SIGNAL(removed(QString)), this, SLOT(removeProfile(QString)));
    
    m_profiles = profiles->listView();
    connect(m_profiles, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editProfile(QModelIndex)));
    
    QWidget* widget = new QWidget(this);
    layout = new QVBoxLayout;
    layout->addWidget(commandGroup);
    layout->addWidget(types);
    layout->addWidget(profiles);
    
    widget->setLayout(layout);
    dialog->setMainWidget(widget);
    dialog->exec();
    
    delete dialog;
}

void EmulatorEditor::addEmulator(const QString& name)
{
    EmuDBConfig::instance()->addEmulator(name, Emulator());
}

void EmulatorEditor::removeEmulator(const QString& name)
{
    EmuDBConfig::instance()->removeEmulator(name);
}

void EmulatorEditor::editProfile(const QModelIndex& index)
{
    editProfile(m_emulator->profile(m_profiles->model()->data(index, Qt::DisplayRole).toString()));
}

void EmulatorEditor::editProfile(Profile* profile)
{
    if (!profile)
        return;
    
    KDialog* dialog = new KDialog(this);
    dialog->setCaption("Edit Profile");
    
    QGroupBox* workingPathGroup = new QGroupBox("Working Path");
    
    KLineEdit* workingPath = new KLineEdit(profile->workingPath());
    KShellCompletion* completion = new KShellCompletion;
    completion->setMode(KUrlCompletion::DirCompletion);
    workingPath->setCompletionObject(completion);
    workingPath->setAutoDeleteCompletionObject(true);
    connect(workingPath, SIGNAL(textChanged(QString)), profile, SLOT(setWorkingPath(QString)));
    
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(workingPath);
    workingPathGroup->setLayout(layout);
    
    KEditListBox* options = new KEditListBox("Options");
    options->setButtons(KEditListBox::Add | KEditListBox::Remove);
    options->setItems(profile->options());
    connect(options, SIGNAL(added(QString)), profile, SLOT(addOption(QString)));
    connect(options, SIGNAL(removed(QString)), profile, SLOT(removeOption(QString)));
    
    KEditListBox* environment = new KEditListBox("Environment");
    environment->setButtons(KEditListBox::Add | KEditListBox::Remove);
    environment->setItems(profile->environmentList());
    connect(environment, SIGNAL(added(QString)), profile, SLOT(addEnvironment(QString)));
    connect(environment, SIGNAL(removed(QString)), profile, SLOT(removeEnvironment(QString)));
    
    QWidget* widget = new QWidget(this);
    layout = new QVBoxLayout;
    layout->addWidget(workingPathGroup);
    layout->addWidget(options);
    layout->addWidget(environment);
    
    widget->setLayout(layout);
    dialog->setMainWidget(widget);
    dialog->exec();
    
    delete dialog;
}

void EmulatorEditor::addProfile(const QString& name)
{
    m_emulator->addProfile(name, Profile());
}

void EmulatorEditor::removeProfile(const QString& name)
{
    m_emulator->removeProfile(name);
}
