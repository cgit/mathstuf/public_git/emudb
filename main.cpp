/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// EmuDB includes
#include "EmuDB.h"
#include "EmuDBConfig.h"

// KDE includes
#include <KAboutData>
#include <KApplication>
#include <KCmdLineArgs>

int main(int argc, char* argv[])
{
    KAboutData about("emudb", "emudb", ki18n("EmuDB"), "0.0.1", ki18n(""), KAboutData::License_GPL_V3, ki18n("©2008 Ben Boeckel"), ki18n(""), "");
    about.setProgramName(ki18n("EmuDB"));
    about.addAuthor(ki18n("Ben Boeckel"), ki18n("Lead Programmer"), "MathStuf@gmail.com", "");
    about.setBugAddress("MathStuf@gmail.com");
    about.setCopyrightStatement(ki18n("2008, Ben Boeckel"));
    about.setOrganizationDomain("");
    // TODO: Need one of these...
//     about.setProgramLogo();
    // TODO: And some of these...
    about.setTranslator(ki18nc("NAME OF TRANSLATORS", "Your names"), ki18nc("EMAIL OF TRANSLATORS", "Your emails"));
    
    KCmdLineArgs::init(argc, argv, &about);
    
    KApplication* app = new KApplication;
    
    EmuDBConfig::instance();
    
    EmuDB* emudb = new EmuDB;
    emudb->show();
    
    const int ret = app->exec();
    
    EmuDBConfig::cleanup();
    delete app;
    
    return ret;
}
